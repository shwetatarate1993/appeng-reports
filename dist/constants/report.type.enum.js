"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ReportType;
(function (ReportType) {
    ReportType["TABLE"] = "table";
    ReportType["GRID"] = "grid";
    ReportType["CUSTOM"] = "custom";
})(ReportType = exports.ReportType || (exports.ReportType = {}));
//# sourceMappingURL=report.type.enum.js.map