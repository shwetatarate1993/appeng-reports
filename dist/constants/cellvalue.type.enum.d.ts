export declare enum CellValueType {
    DATASET_VALUE = "dataset",
    GENERIC_EXPRESSION = "generic",
    NUMERIC_EXPRESSION = "numeric",
    STATIC = "static",
    INPUT = "input",
    AGGREGATION = "aggregation"
}
//# sourceMappingURL=cellvalue.type.enum.d.ts.map