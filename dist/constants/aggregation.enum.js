"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Aggregation;
(function (Aggregation) {
    Aggregation["SUM"] = "sum";
    Aggregation["AVG"] = "avg";
    Aggregation["MIN"] = "min";
    Aggregation["MAX"] = "max";
    Aggregation["COUNT"] = "count";
})(Aggregation = exports.Aggregation || (exports.Aggregation = {}));
//# sourceMappingURL=aggregation.enum.js.map