export declare enum ReportType {
    TABLE = "table",
    GRID = "grid",
    CUSTOM = "custom"
}
//# sourceMappingURL=report.type.enum.d.ts.map