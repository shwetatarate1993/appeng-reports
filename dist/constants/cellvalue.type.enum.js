"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CellValueType;
(function (CellValueType) {
    CellValueType["DATASET_VALUE"] = "dataset";
    CellValueType["GENERIC_EXPRESSION"] = "generic";
    CellValueType["NUMERIC_EXPRESSION"] = "numeric";
    CellValueType["STATIC"] = "static";
    CellValueType["INPUT"] = "input";
    CellValueType["AGGREGATION"] = "aggregation";
})(CellValueType = exports.CellValueType || (exports.CellValueType = {}));
//# sourceMappingURL=cellvalue.type.enum.js.map