"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FileType;
(function (FileType) {
    FileType["EXCEL"] = "excel";
    FileType["PDF"] = "pdf";
})(FileType = exports.FileType || (exports.FileType = {}));
//# sourceMappingURL=file.type.enum.js.map