export declare enum Aggregation {
    SUM = "sum",
    AVG = "avg",
    MIN = "min",
    MAX = "max",
    COUNT = "count"
}
//# sourceMappingURL=aggregation.enum.d.ts.map