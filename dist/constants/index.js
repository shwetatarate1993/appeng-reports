"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var file_type_enum_1 = require("./file.type.enum");
exports.FileType = file_type_enum_1.FileType;
var report_type_enum_1 = require("./report.type.enum");
exports.ReportType = report_type_enum_1.ReportType;
var dbengine_enum_1 = require("./dbengine.enum");
exports.DBEngine = dbengine_enum_1.DBEngine;
exports.parseDBEngineEnum = dbengine_enum_1.parseDBEngineEnum;
var cellvalue_type_enum_1 = require("./cellvalue.type.enum");
exports.CellValueType = cellvalue_type_enum_1.CellValueType;
var aggregation_enum_1 = require("./aggregation.enum");
exports.Aggregation = aggregation_enum_1.Aggregation;
exports.TABLE = 'table';
exports.GRID = 'grid';
exports.DS = 'ds';
exports.DETAIL_START_CELL_INDEX = 'detailStartCellIndex';
exports.DETAIL_END_CELL_INDEX = 'detailEndCellIndex';
exports.DETAIL_START_ROW_INDEX = 'detailStartRowIndex';
exports.DETAIL_END_ROW_INDEX = 'detailEndRowIndex';
exports.SUMMARY_START_CELL_INDEX = 'summaryStartCellIndex';
exports.SUMMARY_END_CELL_INDEX = 'summaryEndCellIndex';
exports.SUMMARY_START_ROW_INDEX = 'summaryStartRowIndex';
exports.SUMMARY_END_ROW_INDEX = 'summaryEndRowIndex';
exports.COLON = ':';
exports.COMMA = ',';
exports.NA = 'NA';
exports.HASH = '#';
exports.GENERIC_PLACEHOLDER = '##generic#';
exports.NUMERIC_PLACEHOLDER = '##numeric#';
exports.INPUT_PLACEHOLDER = '##input#';
// export const TEMPLATE_PATH: string = './dist/templates/';
exports.METADATA_SHEETNAME = 'ae-report-metadata';
// Created like this for JEST
exports.getTemplatePath = () => {
    return './dist/templates/';
};
//# sourceMappingURL=index.js.map