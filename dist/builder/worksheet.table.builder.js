"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const models_1 = require("../models");
const services_1 = require("../services");
const services_2 = require("../services");
const summarycalculator_1 = require("../summarycalculator");
const utils_1 = require("../utils");
class WorksheetTableBuilder {
    async build(metadata, worksheet, reportOptions) {
        const tableDataRows = await fetchData(metadata.data.reportQueries[0].query, reportOptions);
        const dataPlaceHoldersRowIndex = parseInt(metadata.data.location.match(/\d+/)[0], 10);
        const indexMap = await createIndexMap(metadata);
        const summaryMap = new Map();
        if (metadata.summary.location !== constants_1.NA) {
            await initializeSummaryMap(worksheet, indexMap, summaryMap);
        }
        processDetailRows(tableDataRows, reportOptions.params, worksheet, dataPlaceHoldersRowIndex, summaryMap, indexMap);
        if (metadata.summary.location !== constants_1.NA) {
            processSummaryRow(worksheet, indexMap.get(constants_1.SUMMARY_START_ROW_INDEX) + tableDataRows.length, summaryMap, reportOptions.params);
        }
        // Deleting Details MD row
        worksheet.spliceRows(dataPlaceHoldersRowIndex, 1);
    }
}
const createIndexMap = async (metadata) => {
    const tableDetailsStartEndInfo = metadata.location.split(constants_1.COLON);
    const tableSummaryStartEndInfo = metadata.summary.location.split(constants_1.COLON);
    const indexMap = new Map();
    indexMap.set(constants_1.DETAIL_START_CELL_INDEX, parseInt(tableDetailsStartEndInfo[0].split(constants_1.COMMA)[0], 10));
    indexMap.set(constants_1.DETAIL_START_ROW_INDEX, parseInt(tableDetailsStartEndInfo[0].split(constants_1.COMMA)[1], 10));
    indexMap.set(constants_1.DETAIL_END_CELL_INDEX, parseInt(tableDetailsStartEndInfo[1].split(constants_1.COMMA)[0], 10));
    indexMap.set(constants_1.DETAIL_END_ROW_INDEX, parseInt(tableDetailsStartEndInfo[1].split(constants_1.COMMA)[1], 10));
    if (tableSummaryStartEndInfo[0] !== constants_1.NA) {
        indexMap.set(constants_1.SUMMARY_START_CELL_INDEX, parseInt(tableSummaryStartEndInfo[0].split(constants_1.COMMA)[0], 10));
        indexMap.set(constants_1.SUMMARY_START_ROW_INDEX, parseInt(tableSummaryStartEndInfo[0].split(constants_1.COMMA)[1], 10));
        indexMap.set(constants_1.SUMMARY_END_CELL_INDEX, parseInt(tableSummaryStartEndInfo[1].split(constants_1.COMMA)[0], 10));
        indexMap.set(constants_1.SUMMARY_END_ROW_INDEX, parseInt(tableSummaryStartEndInfo[1].split(constants_1.COMMA)[1], 10));
    }
    return indexMap;
};
const fetchData = async (query, reportOptions) => {
    if (query !== constants_1.NA) {
        const response = await services_1.queryService.executeNativeQuery(query, reportOptions);
        return utils_1.formatResponse(response);
    }
    return [];
};
const initializeSummaryMap = async (worksheet, indexMap, summaryMap) => {
    for (let cellIndex = indexMap.get(constants_1.SUMMARY_START_CELL_INDEX); cellIndex <= indexMap.get(constants_1.SUMMARY_END_CELL_INDEX); cellIndex++) {
        const cellValue = worksheet.getRow(indexMap.get(constants_1.SUMMARY_START_ROW_INDEX)).getCell(cellIndex).value;
        await initialize(cellIndex, cellValue, summaryMap);
    }
    return summaryMap;
};
const getAggregation = async (cellValue) => {
    return cellValue.substring(2, cellValue.indexOf('-')).trim();
};
const getSummaryExpression = async (cellValue) => {
    if (/#*sum-|#*avg-|#*min-|#*max-/.test(cellValue)) {
        return cellValue.substring(cellValue.indexOf('-') + 1, cellValue.lastIndexOf(constants_1.HASH)).trim();
    }
    else {
        return cellValue;
    }
};
const initialize = async (cellIndex, cellValue, summaryMap) => {
    if (cellValue) {
        cellValue = cellValue.toString();
        const aggregation = await getAggregation(cellValue);
        const summaryExpr = await getSummaryExpression(cellValue);
        switch (aggregation) {
            case constants_1.Aggregation.SUM:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.AGGREGATION, 0, summaryExpr, constants_1.Aggregation.SUM));
                break;
            case constants_1.Aggregation.AVG:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.AGGREGATION, 0, summaryExpr, constants_1.Aggregation.AVG));
                break;
            case constants_1.Aggregation.MIN:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.AGGREGATION, 0, summaryExpr, constants_1.Aggregation.MIN));
                break;
            case constants_1.Aggregation.MAX:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.AGGREGATION, 0, summaryExpr, constants_1.Aggregation.MAX));
                break;
            case constants_1.CellValueType.GENERIC_EXPRESSION:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.GENERIC_EXPRESSION, '', summaryExpr));
                break;
            case constants_1.CellValueType.NUMERIC_EXPRESSION:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.NUMERIC_EXPRESSION, 0, summaryExpr));
                break;
            case constants_1.CellValueType.INPUT:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.INPUT, '', summaryExpr));
                break;
            default:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.STATIC, cellValue, summaryExpr));
        }
    }
    else {
        summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.STATIC, cellValue));
    }
};
const processDetailRows = async (tableDataRows, inputParams, worksheet, dataPlaceHoldersRowIndex, summaryMap, indexMap) => {
    const data = { params: inputParams, dataRow: {} };
    let dataRowIndex = indexMap.get(constants_1.DETAIL_START_ROW_INDEX) + 1;
    tableDataRows = tableDataRows ? tableDataRows : [];
    // tslint:disable-next-line: prefer-for-of
    for (let rowIndex = 0; rowIndex < tableDataRows.length; rowIndex++) {
        const dataRow = tableDataRows[rowIndex];
        data.dataRow = dataRow;
        worksheet.spliceRows(dataRowIndex, 0, []); // Adding empty row for dataRow details
        for (let columnIndex = indexMap.get(constants_1.DETAIL_START_CELL_INDEX); columnIndex <= indexMap.get(constants_1.DETAIL_END_CELL_INDEX); columnIndex++) {
            let value;
            const cell = worksheet.getRow(indexMap.get(constants_1.DETAIL_START_ROW_INDEX)).getCell(columnIndex);
            if (cell !== null && cell.value !== null) {
                const cellValue = cell.value.toString();
                switch (cellValue.substring(2, cellValue.indexOf(constants_1.HASH, 2)).trim()) {
                    case constants_1.DS:
                        const columnName = cellValue.substring(cellValue.indexOf('${') + 2, cellValue.indexOf('}#')).trim();
                        value = dataRow[columnName] ? dataRow[columnName] : '';
                        break;
                    case constants_1.CellValueType.GENERIC_EXPRESSION:
                        value = services_2.expressionResolver.executeGenericExpressions(data, cellValue);
                        break;
                    case constants_1.CellValueType.NUMERIC_EXPRESSION:
                        value = services_2.expressionResolver.executeNumericExpressions(data, cellValue);
                        break;
                    case constants_1.CellValueType.INPUT:
                        value = services_2.expressionResolver.resolveInputPlaceHoder(data.params, cellValue);
                        break;
                    default:
                        value = cell && cell.value ? cell.value : '';
                }
                worksheet.getRow(dataRowIndex).getCell(columnIndex).value = value;
                worksheet.getRow(dataRowIndex).getCell(columnIndex).style = cell.style;
            }
        }
        if (summaryMap.size) {
            calculateSummaryRow(summaryMap, data, rowIndex);
        }
        dataRowIndex++;
    }
};
const calculateSummaryRow = async (summaryMap, data, rowIndex) => {
    for (const summary of summaryMap.values()) {
        if (summary.cellValueType === constants_1.CellValueType.AGGREGATION) {
            switch (summary.summaryType) {
                case constants_1.Aggregation.SUM:
                    summarycalculator_1.sumCalculator.calculate(summary, data, rowIndex);
                    break;
                case constants_1.Aggregation.AVG:
                    summarycalculator_1.averageCalculator.calculate(summary, data, rowIndex);
                    break;
                case constants_1.Aggregation.MIN:
                    summarycalculator_1.minCalculator.calculate(summary, data, rowIndex);
                    break;
                case constants_1.Aggregation.MAX:
                    summarycalculator_1.maxCalculator.calculate(summary, data, rowIndex);
                    break;
            }
        }
    }
};
const processSummaryRow = async (worksheet, summaryRowIndex, summaryMap, params) => {
    for (const cellIndex of summaryMap.keys()) {
        let value;
        const expr = summaryMap.get(cellIndex).summaryExpression;
        if (expr) {
            switch (expr.substring(2, expr.indexOf(constants_1.HASH, 2)).trim()) {
                case constants_1.CellValueType.GENERIC_EXPRESSION:
                    value = services_2.expressionResolver.executeGenericExpressions(params, expr);
                    summaryMap.get(cellIndex).value = value;
                    break;
                case constants_1.CellValueType.NUMERIC_EXPRESSION:
                    value = services_2.expressionResolver.executeNumericExpressions(params, expr);
                    summaryMap.get(cellIndex).value = value;
                    break;
                case constants_1.CellValueType.INPUT:
                    value = services_2.expressionResolver.resolveInputPlaceHoder(params, expr);
                    summaryMap.get(cellIndex).value = value;
                    break;
            }
        }
        worksheet.getRow(summaryRowIndex).getCell(cellIndex).value = summaryMap.get(cellIndex).value;
    }
};
const builder = new WorksheetTableBuilder();
exports.default = builder;
//# sourceMappingURL=worksheet.table.builder.js.map