"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const worksheet_dynamic_header_table_builder_1 = __importDefault(require("./worksheet.dynamic.header.table.builder"));
const worksheet_grid_builder_1 = __importDefault(require("./worksheet.grid.builder"));
const worksheet_table_builder_1 = __importDefault(require("./worksheet.table.builder"));
class ExcelWorksheetBuilder {
    async build(metadata, workbook, reportOptions) {
        const worksheet = workbook.getWorksheet(metadata.name);
        for (const reportMD of metadata.workSheetReports) {
            switch (reportMD.type) {
                case constants_1.ReportType.TABLE:
                    if (reportMD.header.type === 'fixed') {
                        await worksheet_table_builder_1.default.build(reportMD, worksheet, reportOptions);
                    }
                    else {
                        await worksheet_dynamic_header_table_builder_1.default.build(reportMD, worksheet, reportOptions);
                    }
                    break;
                case constants_1.ReportType.GRID:
                    await worksheet_grid_builder_1.default.build(reportMD, worksheet, reportOptions);
                    break;
                case constants_1.ReportType.CUSTOM:
                    const customFileBuilder = require('./' + metadata.name).default;
                    await customFileBuilder.build(reportMD, worksheet, reportOptions);
                    break;
                default:
                    throw new Error(reportMD.type + ': report type not supported');
            }
        }
    }
}
const builder = new ExcelWorksheetBuilder();
exports.default = builder;
//# sourceMappingURL=excel.worksheet.builder.js.map