"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const models_1 = require("../models");
const services_1 = require("../services");
const services_2 = require("../services");
const utils_1 = require("../utils");
class WorksheetDynamicHeaderTableBuilder {
    async build(metadata, worksheet, reportOptions) {
        const headers = await fetchData(metadata.header.reportQueries[0].query, reportOptions);
        const tableDataRows = await fetchData(metadata.data.reportQueries[0].query, reportOptions);
        const summaryMap = new Map();
        if (metadata.summary.location !== constants_1.NA) {
            await initializeSummaryMap(worksheet, reportOptions.params, metadata, headers, summaryMap);
        }
        const map = new Map();
        processHeader(headers, metadata, worksheet, map);
        processDetails(tableDataRows, metadata, worksheet, headers, map);
        // Deleting Details MD row
        worksheet.spliceRows(parseInt(metadata.location.split(constants_1.COMMA)[1], 10), 1);
    }
}
const processHeader = (headers, metadata, worksheet, map) => {
    const tableHeaderStartInfo = metadata.header.location.split(constants_1.COMMA);
    const headerStartCellIndex = parseInt(tableHeaderStartInfo[0], 10);
    const headerStartRowIndex = parseInt(tableHeaderStartInfo[1], 10);
    const headerRow = worksheet.getRow(headerStartRowIndex);
    const headerStyle = worksheet.getRow(headerStartRowIndex).getCell(headerStartCellIndex).style;
    for (let columnIndex = headerStartCellIndex; columnIndex < (headerStartCellIndex + headers.length); columnIndex++) {
        const header = headers[columnIndex - headerStartCellIndex];
        headerRow.getCell(columnIndex).value = header[Object.keys(header)[0]];
        headerRow.getCell(columnIndex).style = headerStyle;
        map.set(columnIndex, header[Object.keys(header)[0]]);
    }
};
const processDetails = (tableDataRows, metadata, worksheet, headers, map) => {
    const tableDetailsStartInfo = metadata.location.split(constants_1.COMMA);
    const detailStartCellIndex = parseInt(tableDetailsStartInfo[0], 10);
    const detailStartRowIndex = parseInt(tableDetailsStartInfo[1], 10);
    let dataRowIndex = detailStartRowIndex + 1;
    const detailStyle = worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex).style;
    const detailCellKey = worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex).value.toString();
    tableDataRows = createDetailJsonWithHeaderNameAsKey(headers, tableDataRows);
    for (const tableDataRow of tableDataRows) {
        worksheet.spliceRows(dataRowIndex, 0, []); // Adding empty row for dataRow details
        for (let columnIndex = detailStartCellIndex; columnIndex < detailStartCellIndex + headers.length; columnIndex++) {
            const record = tableDataRow[map.get(columnIndex).toString()];
            worksheet.getRow(dataRowIndex).getCell(columnIndex).value = record ? record[detailCellKey] : '';
            worksheet.getRow(dataRowIndex).getCell(columnIndex).style = detailStyle;
        }
        dataRowIndex++;
    }
};
const createDetailJsonWithHeaderNameAsKey = (headers, tableDataRows) => {
    const detailRecords = [];
    const headerKey = Object.keys(headers[0])[0];
    for (const dataRecord of tableDataRows) {
        detailRecords.push({ [dataRecord[headerKey]]: dataRecord });
    }
    return detailRecords;
};
const fetchData = async (query, reportOptions) => {
    if (query !== constants_1.NA) {
        const response = await services_1.queryService.executeNativeQuery(query, reportOptions);
        return utils_1.formatResponse(response);
    }
    return [];
};
const initializeSummaryMap = async (worksheet, inputParams, metadata, headers, summaryMap) => {
    const tableSummaryStartInfo = metadata.summary.location.split(constants_1.COMMA);
    const summaryStartCellIndex = parseInt(tableSummaryStartInfo[0], 10);
    const summaryStartRowIndex = parseInt(tableSummaryStartInfo[1], 10);
    for (let cellIndex = summaryStartCellIndex; cellIndex < summaryStartCellIndex + headers.length; cellIndex++) {
        const cellValue = worksheet.getRow(summaryStartRowIndex).getCell(cellIndex).value;
        await initialize(cellIndex, cellValue, summaryMap, inputParams);
    }
    return summaryMap;
};
const initialize = async (cellIndex, cellValue, summaryMap, inputParams) => {
    if (cellValue !== null && cellValue !== undefined) {
        cellValue = cellValue.toString();
        switch (cellValue.substring(2, cellValue.indexOf(constants_1.HASH, 2)).trim()) {
            case constants_1.Aggregation.SUM:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.AGGREGATION, 0, constants_1.Aggregation.SUM));
                break;
            case constants_1.Aggregation.AVG:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.AGGREGATION, 0, constants_1.Aggregation.AVG));
                break;
            case constants_1.Aggregation.MIN:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.AGGREGATION, 0, constants_1.Aggregation.MIN));
                break;
            case constants_1.Aggregation.MAX:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.AGGREGATION, 0, constants_1.Aggregation.MAX));
                break;
            case constants_1.CellValueType.GENERIC_EXPRESSION:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.GENERIC_EXPRESSION, '', cellValue));
                break;
            case constants_1.CellValueType.NUMERIC_EXPRESSION:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.NUMERIC_EXPRESSION, 0, cellValue));
                break;
            case constants_1.CellValueType.INPUT:
                cellValue = services_2.expressionResolver.resolveInputPlaceHoder(inputParams, cellValue);
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.INPUT, '', cellValue));
                break;
            default:
                summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.STATIC, '', cellValue));
        }
    }
    else {
        summaryMap.set(cellIndex, new models_1.Summary(constants_1.CellValueType.STATIC, ''));
    }
};
const builder = new WorksheetDynamicHeaderTableBuilder();
exports.default = builder;
//# sourceMappingURL=worksheet.dynamic.header.table.builder.js.map