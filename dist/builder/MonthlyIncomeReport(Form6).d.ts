import Excel from 'exceljs';
import { WorkSheetReport } from '../metadata/models';
import { ReportOptions } from '../models';
declare class Form6Builder {
    build(metadata: WorkSheetReport, worksheet: Excel.Worksheet, reportOptions: ReportOptions): Promise<void>;
}
declare const form6Builder: Form6Builder;
export default form6Builder;
//# sourceMappingURL=MonthlyIncomeReport(Form6).d.ts.map