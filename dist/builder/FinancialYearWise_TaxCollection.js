"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const services_1 = require("../services");
const utils_1 = require("../utils");
class Form9Builder {
    async build(metadata, worksheet, reportOptions) {
        const tableHeaderStartInfo = metadata.header.location.split(constants_1.COMMA);
        const headerStartCellIndex = parseInt(tableHeaderStartInfo[0], 10);
        const headerStartRowIndex = parseInt(tableHeaderStartInfo[1], 10);
        const headers = await fetchData(metadata.header.reportQueries[0].query, reportOptions);
        await processHeader(headers, headerStartRowIndex, headerStartCellIndex, worksheet);
        const detailMap = await getDetailMap(worksheet, reportOptions);
        await processDetailRows(worksheet, metadata, headerStartRowIndex, headerStartCellIndex, headers, detailMap);
        // Deleting Details MD row
        worksheet.spliceRows(parseInt(metadata.location.split(constants_1.COMMA)[1], 10), 1);
    }
}
const processHeader = async (headers, headerStartRowIndex, headerStartCellIndex, worksheet) => {
    const headerStyle = worksheet.getRow(headerStartRowIndex).getCell(headerStartCellIndex).style;
    await createDemandHeaderSection(headerStartCellIndex, headerStartRowIndex, headerStyle, headers, worksheet);
    let totalDemandCellIndex = headerStartCellIndex + (headers.length * 3);
    worksheet.getRow(headerStartRowIndex - 1).getCell(totalDemandCellIndex).style = headerStyle;
    worksheet.getRow(headerStartRowIndex).getCell(totalDemandCellIndex).style = headerStyle;
    worksheet.mergeCells(headerStartRowIndex - 1, totalDemandCellIndex, headerStartRowIndex + 1, totalDemandCellIndex);
    worksheet.getRow(headerStartRowIndex - 1).getCell(totalDemandCellIndex + 1).value = 'પહોચા નંબર ને તારીખ (Receipt Detail)';
    worksheet.getRow(headerStartRowIndex - 1).getCell(totalDemandCellIndex + 1).style = headerStyle;
    worksheet.getRow(headerStartRowIndex - 1).getCell(totalDemandCellIndex + 2).style = headerStyle;
    worksheet.mergeCells(headerStartRowIndex - 1, totalDemandCellIndex + 1, headerStartRowIndex, totalDemandCellIndex + 2);
    const headerKeys = ['કૂલ માગણું (Total Demand)', 'તારીખ (Date)', 'પહોચ (Receipt No)'];
    for (const header of headerKeys) {
        worksheet.getRow(headerStartRowIndex + 1).getCell(totalDemandCellIndex++).value = header;
        worksheet.getRow(headerStartRowIndex + 1).getCell(totalDemandCellIndex).style = headerStyle;
    }
    await createRecoveryHeaderSection(totalDemandCellIndex, headerStartRowIndex, headerStyle, headers, worksheet);
    const remainSectionStartCellIndex = totalDemandCellIndex + (headers.length * 3) + 1;
    await createHeaderSection('બાકી (Remain)', remainSectionStartCellIndex, headerStartRowIndex, headerStyle, headers, worksheet);
    const depositedSectionStartCellIndex = remainSectionStartCellIndex + headers.length;
    await createHeaderSection('જમા (Deposited)', depositedSectionStartCellIndex, headerStartRowIndex, headerStyle, headers, worksheet);
    // Last Remark Cell
    const remarkCellIndex = depositedSectionStartCellIndex + headers.length;
    worksheet.getRow(headerStartRowIndex - 1).getCell(remarkCellIndex).value = 'શેરો (Remark)';
    worksheet.getRow(headerStartRowIndex - 1).getCell(remarkCellIndex).style = headerStyle;
    worksheet.mergeCells(headerStartRowIndex - 1, remarkCellIndex, headerStartRowIndex + 1, remarkCellIndex);
};
const getDetailMap = async (worksheet, reportOptions) => {
    const dataQuery = 'mysql!lAnGuAge!SELECT pts.PROPERTY_UUID, pts.PROPERTY_NUMBER,concat(resident.RESIDENT_FIRST_NAME, " ", resident.RESIDENT_LAST_NAME) as OWNER_NAME,tax.TAX_NAME,ptd.PROPERTY_PREVIOUS_PENDING_TAX_AMOUNT as PREV_DEMAND,(ptd.PROPERTY_CURRENT_YEAR_PENDING_TAX_AMOUNT + ptd.PROPERTY_ADJUSTMENT_TAX_AMOUNT - ptd.PROPERTY_ADVANCE_TAX_AMOUNT) as CURR_DEMAND,ptd.TOTAL_PROPERTY_TAX_AMOUNT as TOTAL_DEMAND, ptd.TOTAL_PREVIOUS_YEAR_TAX_PAID_AMT as PREV_RECOVERY,ptd.TOTAL_CURRENT_YEAR_TAX_PAID_AMT as CURR_RECOVERY,ptd.PROPERTY_TAX_PAID_AMOUNT as TOTAL_RECOVERY,ptd.PROPERTY_NET_BALANCE_TAX_AMOUNT as REMAIN,ptd.PROPERTY_NET_ADVANCE_TAX_AMOUNT as DEPOSITED, property.REMARKS FROM PROPERTY_TAX_DETAIL ptd left join PROPERTY_TAX_SUMMARY pts on (ptd.PROPERTY_TAX_SUMMARY_UUID = pts.PROPERTY_TAX_SUMMARY_UUID) left join RESIDENT resident on (pts.RESIDENT_UUID=resident.RESIDENT_UUID) left join GRAM_PANCHAYAT_FINANCIAL_YEAR_TAX_RATE tax on (ptd.GRAM_PANCHAYAT_FINANCIAL_YEAR_TAX_RATE_UUID = tax.GRAM_PANCHAYAT_FINANCIAL_YEAR_TAX_RATE_UUID) left join PROPERTY property on (pts.PROPERTY_UUID = property.PROPERTY_UUID) where pts.FINANCIAL_YEAR_ID=:APP_LOGGED_IN_YEAR and pts.GRAMPANCHAYAT_ID=:APP_LOGGED_IN_USER_GP sEpArAToRsqlite3!lAnGuAge! SELECT pts.PROPERTY_UUID, pts.PROPERTY_NUMBER,(resident.RESIDENT_FIRST_NAME || " " || resident.RESIDENT_LAST_NAME) as OWNER_NAME,tax.TAX_NAME,ptd.PROPERTY_PREVIOUS_PENDING_TAX_AMOUNT as PREV_DEMAND,(ptd.PROPERTY_CURRENT_YEAR_PENDING_TAX_AMOUNT + ptd.PROPERTY_ADJUSTMENT_TAX_AMOUNT - ptd.PROPERTY_ADVANCE_TAX_AMOUNT) as CURR_DEMAND,ptd.TOTAL_PROPERTY_TAX_AMOUNT as TOTAL_DEMAND,ptd.TOTAL_PREVIOUS_YEAR_TAX_PAID_AMT as PREV_RECOVERY,ptd.TOTAL_CURRENT_YEAR_TAX_PAID_AMT as CURR_RECOVERY,ptd.PROPERTY_TAX_PAID_AMOUNT as TOTAL_RECOVERY,ptd.PROPERTY_NET_BALANCE_TAX_AMOUNT as REMAIN,ptd.PROPERTY_NET_ADVANCE_TAX_AMOUNT as DEPOSITED, property.REMARKS FROM PROPERTY_TAX_DETAIL ptd left join PROPERTY_TAX_SUMMARY pts on (ptd.PROPERTY_TAX_SUMMARY_UUID = pts.PROPERTY_TAX_SUMMARY_UUID) left join RESIDENT resident on (pts.RESIDENT_UUID=resident.RESIDENT_UUID) left join GRAM_PANCHAYAT_FINANCIAL_YEAR_TAX_RATE tax on (ptd.GRAM_PANCHAYAT_FINANCIAL_YEAR_TAX_RATE_UUID = tax.GRAM_PANCHAYAT_FINANCIAL_YEAR_TAX_RATE_UUID) left join PROPERTY property on (pts.PROPERTY_UUID = property.PROPERTY_UUID) where pts.FINANCIAL_YEAR_ID=:APP_LOGGED_IN_YEAR and pts.GRAMPANCHAYAT_ID=:APP_LOGGED_IN_USER_GP';
    const detailRows = await fetchData(dataQuery, reportOptions);
    const detailMap = new Map();
    let taxMap;
    const propertyNumber = [];
    for (const detailRow of detailRows) {
        const taxDetailMap = new Map();
        taxDetailMap.set('PREV_DEMAND', detailRow.PREV_DEMAND);
        taxDetailMap.set('CURR_DEMAND', detailRow.CURR_DEMAND);
        taxDetailMap.set('TOTAL_DEMAND', detailRow.TOTAL_DEMAND);
        reportOptions.params.PROPERTY_UUID = detailRow.PROPERTY_UUID;
        const selectDateAndReceiptNumber = 'mysql!lAnGuAge!select tcs.TRANSACTION_DATE, tcs.RECEIPT_NUMBER from TAX_COLLECTION_SUMMARY tcs where tcs.PROPERTY_UUID=:PROPERTY_UUID and tcs.FINANCIAL_YEAR_ID=:APP_LOGGED_IN_YEAR and tcs.GRAMPANCHAYAT_ID=:APP_LOGGED_IN_USER_GP order by TRANSACTION_DATE desc limit 1 sEpArAToRsqlite3!lAnGuAge! select tcs.TRANSACTION_DATE, tcs.RECEIPT_NUMBER from TAX_COLLECTION_SUMMARY tcs where tcs.PROPERTY_UUID=:PROPERTY_UUID and tcs.FINANCIAL_YEAR_ID=:APP_LOGGED_IN_YEAR and tcs.GRAMPANCHAYAT_ID=:APP_LOGGED_IN_USER_GP order by TRANSACTION_DATE desc limit 1';
        const dateAndReceiptNumber = await fetchData(selectDateAndReceiptNumber, reportOptions);
        taxDetailMap.set('DATE', dateAndReceiptNumber.length ? dateAndReceiptNumber[0].TRANSACTION_DATE : '');
        taxDetailMap.set('RECEIPT_NUMBER', dateAndReceiptNumber.length ? dateAndReceiptNumber[0].RECEIPT_NUMBER : '');
        taxDetailMap.set('PREV_RECOVERY', detailRow.PREV_RECOVERY);
        taxDetailMap.set('CURR_RECOVERY', detailRow.CURR_RECOVERY);
        taxDetailMap.set('TOTAL_RECOVERY', detailRow.TOTAL_RECOVERY);
        taxDetailMap.set('REMAIN', detailRow.REMAIN);
        taxDetailMap.set('DEPOSITED', detailRow.DEPOSITED);
        taxDetailMap.set('REMARKS', detailRow.REMARKS);
        if (!propertyNumber.includes(detailRow.PROPERTY_NUMBER)) {
            propertyNumber.push(detailRow.PROPERTY_NUMBER);
            taxMap = new Map();
        }
        taxMap.set('OWNER_NAME', new Map([['OWNER_NAME', detailRow.OWNER_NAME]]));
        taxMap.set(detailRow.TAX_NAME, taxDetailMap);
        detailMap.set(detailRow.PROPERTY_NUMBER, taxMap);
    }
    return detailMap;
};
const processDetailRows = async (worksheet, metadata, headerStartRowIndex, headerStartCellIndex, headers, detailRowsMap) => {
    const tableDetailsStartInfo = metadata.data.location.split(constants_1.COMMA);
    const detailStartCellIndex = parseInt(tableDetailsStartInfo[0], 10);
    let detailStartRowIndex = parseInt(tableDetailsStartInfo[1], 10);
    const srNoStyle = worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex).style;
    const leftAlignDetailStyle = worksheet.getRow(detailStartRowIndex).getCell(3).style;
    const rightAlignDetailStyle = worksheet.getRow(detailStartRowIndex).getCell(5).style;
    const centerAlignDetailStyle = worksheet.getRow(detailStartRowIndex).getCell(6).style;
    const rightAlignBoldDetailStyle = worksheet.getRow(detailStartRowIndex).getCell(7).style;
    const demandStartCellIndex = 5;
    const grandTotalDemandCellIndex = demandStartCellIndex + (headers.length * 3);
    const dateCellIndex = demandStartCellIndex + (headers.length * 3) + 1;
    const receiptNoCellIndex = demandStartCellIndex + (headers.length * 3) + 2;
    const recoveryStartCellIndex = demandStartCellIndex + (headers.length * 3) + 3;
    const grandTotalRecoveryCellIndex = recoveryStartCellIndex + (headers.length * 3);
    const remainStartCellIndex = recoveryStartCellIndex + (headers.length * 3) + 1;
    const depositedStartCellIndex = remainStartCellIndex + headers.length;
    const remarkCellIndex = depositedStartCellIndex + headers.length;
    for (const detailRowMap of detailRowsMap) {
        ++detailStartRowIndex;
        // Filling Default Zero: All static header + headers for Demand & Recovery + headers for Remain & Deposited
        const endCellIndex = 9 + ((headers.length * 3) * 2) + (headers.length * 2);
        for (let i = 5; i <= endCellIndex; i++) {
            worksheet.getRow(detailStartRowIndex).getCell(i).value = 0;
            worksheet.getRow(detailStartRowIndex).getCell(i).style = rightAlignDetailStyle;
        }
        worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex).value = detailStartRowIndex - 7;
        worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex).style = srNoStyle;
        worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex + 1).value = detailRowMap[0];
        worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex + 1).style = leftAlignDetailStyle;
        worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex + 2).value = detailRowMap[1]
            .get('OWNER_NAME').get('OWNER_NAME').toString();
        worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex + 2).style = leftAlignDetailStyle;
        detailRowMap[1].delete('OWNER_NAME');
        const taxList = [];
        for (const header of headers) {
            taxList.push(header.TAX_NAME);
        }
        let grandTotalDemand = 0;
        let grandTotalRecovery = 0;
        for (const taxDetailMap of detailRowMap[1].entries()) {
            const taxIndex = taxList.indexOf(taxDetailMap[0]);
            const taxDetailsMap = taxDetailMap[1];
            // Setting Demand Taxes Details
            worksheet.getRow(detailStartRowIndex).getCell(demandStartCellIndex + (3 * taxIndex)).value =
                taxDetailsMap.get('PREV_DEMAND') ? taxDetailsMap.get('PREV_DEMAND') : 0;
            worksheet.getRow(detailStartRowIndex).getCell(demandStartCellIndex + (3 * taxIndex)).style =
                rightAlignDetailStyle;
            worksheet.getRow(detailStartRowIndex).getCell(demandStartCellIndex + 1 + (3 * taxIndex)).value =
                taxDetailsMap.get('CURR_DEMAND') ? taxDetailsMap.get('CURR_DEMAND') : 0;
            worksheet.getRow(detailStartRowIndex).getCell(demandStartCellIndex + 1 + (3 * taxIndex)).style =
                rightAlignDetailStyle;
            const totalDemand = taxDetailsMap.get('TOTAL_DEMAND') ?
                parseFloat(taxDetailsMap.get('TOTAL_DEMAND')) : 0;
            grandTotalDemand = grandTotalDemand + totalDemand;
            worksheet.getRow(detailStartRowIndex).getCell(demandStartCellIndex + 2 + (3 * taxIndex)).value =
                totalDemand;
            worksheet.getRow(detailStartRowIndex).getCell(demandStartCellIndex + 2 + (3 * taxIndex)).style =
                rightAlignDetailStyle;
            // Setting Date
            worksheet.getRow(detailStartRowIndex).getCell(dateCellIndex).value = taxDetailsMap.get('DATE');
            worksheet.getRow(detailStartRowIndex).getCell(dateCellIndex).style = centerAlignDetailStyle;
            // Setting Receipt Number
            worksheet.getRow(detailStartRowIndex).getCell(receiptNoCellIndex).value = taxDetailsMap
                .get('RECEIPT_NUMBER') ? taxDetailsMap.get('RECEIPT_NUMBER') : '';
            worksheet.getRow(detailStartRowIndex).getCell(receiptNoCellIndex).style = leftAlignDetailStyle;
            // Setting Recovery Taxes Details
            worksheet.getRow(detailStartRowIndex).getCell(recoveryStartCellIndex + (3 * taxIndex)).value =
                taxDetailsMap.get('PREV_RECOVERY') ? taxDetailsMap.get('PREV_RECOVERY') : 0;
            worksheet.getRow(detailStartRowIndex).getCell(recoveryStartCellIndex + (3 * taxIndex)).style =
                rightAlignDetailStyle;
            worksheet.getRow(detailStartRowIndex).getCell(recoveryStartCellIndex + 1 + (3 * taxIndex)).value =
                taxDetailsMap.get('CURR_RECOVERY') ? taxDetailsMap.get('CURR_RECOVERY') : 0;
            worksheet.getRow(detailStartRowIndex).getCell(recoveryStartCellIndex + 1 + (3 * taxIndex)).style =
                rightAlignDetailStyle;
            const totalRecovery = taxDetailsMap.get('TOTAL_RECOVERY') ?
                parseFloat(taxDetailsMap.get('TOTAL_RECOVERY')) : 0;
            grandTotalRecovery = grandTotalRecovery + totalRecovery;
            worksheet.getRow(detailStartRowIndex).getCell(recoveryStartCellIndex + 2 + (3 * taxIndex)).value =
                totalRecovery;
            worksheet.getRow(detailStartRowIndex).getCell(recoveryStartCellIndex + 2 + (3 * taxIndex)).style =
                rightAlignDetailStyle;
            // Setting Remain Details
            worksheet.getRow(detailStartRowIndex).getCell(remainStartCellIndex + taxIndex).value =
                taxDetailsMap.get('REMAIN') ? taxDetailsMap.get('REMAIN') : 0;
            worksheet.getRow(detailStartRowIndex).getCell(remainStartCellIndex + taxIndex).style =
                rightAlignDetailStyle;
            // Setting Deposited Details
            worksheet.getRow(detailStartRowIndex).getCell(depositedStartCellIndex + taxIndex).value =
                taxDetailsMap.get('DEPOSITED') ? taxDetailsMap.get('DEPOSITED') : 0;
            worksheet.getRow(detailStartRowIndex).getCell(depositedStartCellIndex + taxIndex).style =
                rightAlignDetailStyle;
            // Setting Remarks
            worksheet.getRow(detailStartRowIndex).getCell(remarkCellIndex).value = taxDetailsMap
                .get('REMARKS') ? taxDetailsMap.get('REMARKS') : '';
            worksheet.getRow(detailStartRowIndex).getCell(remarkCellIndex).style = leftAlignDetailStyle;
        }
        // Setting GrandTotalDemand
        worksheet.getRow(detailStartRowIndex).getCell(grandTotalDemandCellIndex).value = grandTotalDemand;
        worksheet.getRow(detailStartRowIndex).getCell(grandTotalDemandCellIndex).style = rightAlignBoldDetailStyle;
        // Setting GrandTotalRecovery
        worksheet.getRow(detailStartRowIndex).getCell(grandTotalRecoveryCellIndex).value = grandTotalRecovery;
        worksheet.getRow(detailStartRowIndex).getCell(grandTotalRecoveryCellIndex).style = rightAlignBoldDetailStyle;
    }
};
const createDemandHeaderSection = async (headerStartCellIndex, headerStartRowIndex, headerStyle, headers, worksheet) => {
    let columnIndex;
    let i = 0;
    for (columnIndex = headerStartCellIndex; columnIndex < (headerStartCellIndex + (headers.length * 3)); columnIndex += 3) {
        // Creating Demand Header 2nd row i.e Taxes Name
        worksheet.getRow(headerStartRowIndex).getCell(columnIndex).value = headers[i].TAX_NAME;
        worksheet.getRow(headerStartRowIndex).getCell(columnIndex).style = headerStyle;
        worksheet.mergeCells(headerStartRowIndex, columnIndex, headerStartRowIndex, columnIndex + 2);
        i++;
        // Creating Demand Header 3rd row i.e Previous, Current, Total
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex).value = 'બાકી (Previous)';
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex).style = headerStyle;
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex + 1).value = 'ચાલુ (Current)';
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex + 1).style = headerStyle;
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex + 2).value = 'કૂલ (Total)';
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex + 2).style = headerStyle;
    }
    // Top Demand Cell
    worksheet.getRow(headerStartRowIndex - 1).getCell(headerStartCellIndex).value = 'માંગણું (Demand)';
    worksheet.getRow(headerStartRowIndex - 1).getCell(headerStartCellIndex).style = headerStyle;
    worksheet.mergeCells(headerStartRowIndex - 1, headerStartCellIndex, headerStartRowIndex - 1, columnIndex - 1);
};
const createRecoveryHeaderSection = async (startCellIndex, headerStartRowIndex, headerStyle, headers, worksheet) => {
    let columnIndex;
    let i = 0;
    for (columnIndex = startCellIndex; columnIndex < (startCellIndex + (headers.length * 3)); columnIndex += 3) {
        // Creating Demand Header 2nd row i.e Taxes Name
        worksheet.getRow(headerStartRowIndex).getCell(columnIndex).value = headers[i].TAX_NAME;
        worksheet.getRow(headerStartRowIndex).getCell(columnIndex).style = headerStyle;
        worksheet.mergeCells(headerStartRowIndex, columnIndex, headerStartRowIndex, columnIndex + 2);
        i++;
        // Creating Demand Header 3rd row i.e Previous, Current, Total
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex).value = 'બાકી (Previous)';
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex).style = headerStyle;
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex + 1).value = 'ચાલુ (Current';
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex + 1).style = headerStyle;
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex + 2).value = 'કૂલ (Total)';
        worksheet.getRow(headerStartRowIndex + 1).getCell(columnIndex + 2).style = headerStyle;
    }
    // Top Recovery Cell
    worksheet.getRow(headerStartRowIndex - 1).getCell(startCellIndex).value = 'વસુલાત (Recovery)';
    worksheet.getRow(headerStartRowIndex - 1).getCell(startCellIndex).style = headerStyle;
    worksheet.mergeCells(headerStartRowIndex - 1, startCellIndex, headerStartRowIndex - 1, columnIndex - 1);
    worksheet.getRow(headerStartRowIndex - 1).getCell(columnIndex).value = 'કૂલ વસુલાત (Total Recovery)';
    for (i = headerStartRowIndex - 1; i < headerStartRowIndex + 2; i++) {
        worksheet.getRow(i).getCell(columnIndex).style = headerStyle;
    }
    worksheet.mergeCells(headerStartRowIndex - 1, columnIndex, headerStartRowIndex + 1, columnIndex);
};
const createHeaderSection = async (headerValue, startCellIndex, headerStartRowIndex, headerStyle, headers, worksheet) => {
    let columnIndex;
    let i = 0;
    for (columnIndex = startCellIndex; columnIndex < (startCellIndex + headers.length); columnIndex++) {
        worksheet.getRow(headerStartRowIndex).getCell(columnIndex).value = headers[i].TAX_NAME;
        worksheet.getRow(headerStartRowIndex).getCell(columnIndex).style = headerStyle;
        worksheet.mergeCells(headerStartRowIndex, columnIndex, headerStartRowIndex + 1, columnIndex);
        i++;
    }
    worksheet.getRow(headerStartRowIndex - 1).getCell(startCellIndex).value = headerValue;
    worksheet.getRow(headerStartRowIndex - 1).getCell(startCellIndex).style = headerStyle;
    worksheet.mergeCells(headerStartRowIndex - 1, startCellIndex, headerStartRowIndex - 1, columnIndex - 1);
};
const fetchData = async (query, reportOptions) => {
    if (query !== constants_1.NA) {
        const response = await services_1.queryService.executeNativeQuery(query, reportOptions);
        return utils_1.formatResponse(response);
    }
    return [];
};
const form9Builder = new Form9Builder();
Object.freeze(form9Builder);
exports.default = form9Builder;
//# sourceMappingURL=FinancialYearWise_TaxCollection.js.map