import { Workbook } from 'exceljs';
import { ExcelWorkSheet } from '../metadata/models';
import { ReportOptions } from '../models';
declare class ExcelWorksheetBuilder {
    build(metadata: ExcelWorkSheet, workbook: Workbook, reportOptions: ReportOptions): Promise<void>;
}
declare const builder: ExcelWorksheetBuilder;
export default builder;
//# sourceMappingURL=excel.worksheet.builder.d.ts.map