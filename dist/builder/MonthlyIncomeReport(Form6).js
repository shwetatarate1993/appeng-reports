"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const constants_1 = require("../constants");
const services_1 = require("../services");
const utils_1 = require("../utils");
class Form6Builder {
    async build(metadata, worksheet, reportOptions) {
        const headers = await fetchData(metadata.header.reportQueries[0].query, reportOptions);
        await processHeader(headers, metadata, worksheet);
        await processDetails(metadata, worksheet, headers, reportOptions);
        // Deleting Details MD row
        worksheet.spliceRows(parseInt(metadata.location.split(constants_1.COMMA)[1], 10), 1);
    }
}
const processHeader = async (headers, metadata, worksheet) => {
    const tableHeaderStartInfo = metadata.header.location.split(constants_1.COMMA);
    const headerStartCellIndex = parseInt(tableHeaderStartInfo[0], 10);
    const headerStartRowIndex = parseInt(tableHeaderStartInfo[1], 10);
    const headerRow = worksheet.getRow(headerStartRowIndex);
    const headerStyle = worksheet.getRow(headerStartRowIndex).getCell(headerStartCellIndex).style;
    // Merging Top Header Cell i.e Date Label
    worksheet.mergeCells(9, 3, 9, 3 + headers.length - 1);
    // Creating Dates in Header
    let columnIndex;
    for (columnIndex = headerStartCellIndex; columnIndex < (headerStartCellIndex + headers.length); columnIndex++) {
        const header = headers[columnIndex - headerStartCellIndex];
        headerRow.getCell(columnIndex).value = new Date(header.dayOfDate).getDate();
        headerRow.getCell(columnIndex).style = headerStyle;
    }
    // Creating last 3 Summary Columns in Header
    const summaryHeaders = ['મહીનાનો સરવાળો (Total of Current month)', 'પાછલા મહીના સરવાળો (Total of Previous month)', 'એકદર કૂલ (Total)'];
    for (const summaryHeader of summaryHeaders) {
        headerRow.getCell(columnIndex).style = headerStyle;
        worksheet.getRow(headerStartRowIndex - 1).getCell(columnIndex).style = headerStyle;
        worksheet.mergeCells(headerStartRowIndex - 1, columnIndex, headerStartRowIndex, columnIndex);
        headerRow.getCell(columnIndex).value = summaryHeader;
        columnIndex++;
    }
};
const processDetails = async (metadata, worksheet, headers, reportOptions) => {
    const taxMap = new Map();
    let form6DetailsMapList = [];
    for (const header of headers) {
        const date = header.dayOfDate;
        reportOptions.params.TRANSACTION_DATE = moment_1.default(date).format('YYYY/MM/DD');
        form6DetailsMapList = await fetchData(metadata.data.reportQueries[0].query, reportOptions);
        await createMapToProcessDetailsData(form6DetailsMapList, taxMap, date, header);
    }
    const selectPreviousMonthTotalIncomeForTaxes = 'mysql!lAnGuAge!SELECT head.HEAD_NAME, SUM(details.INCOME_AMOUNT) as TOTAL_OF_PREVIOUS_MONTH FROM INCOME_DETAILS details left join INCOME_SUMMARY incomeSummary on (details.INCOME_SUMMARY_UUID=incomeSummary.INCOME_SUMMARY_UUID) left join INCOME_EXPENSE_HEAD head on (details.INCOME_HEAD_UUID=head.INCOME_EXPENSE_HEAD_UUID) where MONTH(incomeSummary.TRANSACTION_DATE) = MONTH((DATE_SUB(concat(:YEAR,"-",:MONTH,"-01"), INTERVAL 1 MONTH))) and YEAR(incomeSummary.TRANSACTION_DATE) = YEAR((DATE_SUB(concat(:YEAR,"-",:MONTH,"-01"), INTERVAL 1 MONTH))) and incomeSummary.GRAMPANCHAYAT_ID =:APP_LOGGED_IN_USER_GP and incomeSummary.FINANCIAL_YEAR_ID =:APP_LOGGED_IN_YEAR group by head.HEAD_NAME sEpArAToRsqlite3!lAnGuAge! SELECT head.HEAD_NAME, SUM(details.INCOME_AMOUNT) as TOTAL_OF_PREVIOUS_MONTH FROM INCOME_DETAILS details left join INCOME_SUMMARY incomeSummary on (details.INCOME_SUMMARY_UUID=incomeSummary.INCOME_SUMMARY_UUID) left join INCOME_EXPENSE_HEAD head on (details.INCOME_HEAD_UUID=head.INCOME_EXPENSE_HEAD_UUID) where strftime("%m", incomeSummary.TRANSACTION_DATE) = strftime("%m", datetime(:YEAR||"-"||:MONTH||"-01", "-1 month", "start of month")) and strftime("%Y", incomeSummary.TRANSACTION_DATE) = strftime("%Y", datetime(:YEAR||"-"||:MONTH||"-01", "-1 month", "start of month")) and incomeSummary.GRAMPANCHAYAT_ID =:APP_LOGGED_IN_USER_GP and incomeSummary.FINANCIAL_YEAR_ID =:APP_LOGGED_IN_YEAR group by head.HEAD_NAME';
    const prevMonthTotalIncomeAmtForTaxes = await fetchData(selectPreviousMonthTotalIncomeForTaxes, reportOptions);
    const taxTotalPrevAmtMap = new Map();
    for (const prevMonthTotalIncomeAmtForTax of prevMonthTotalIncomeAmtForTaxes) {
        if (prevMonthTotalIncomeAmtForTax.HEAD_NAME) {
            taxTotalPrevAmtMap.set(prevMonthTotalIncomeAmtForTax.HEAD_NAME, prevMonthTotalIncomeAmtForTax.TOTAL_OF_PREVIOUS_MONTH);
        }
    }
    const tableDetailsStartInfo = metadata.location.split(constants_1.COMMA);
    const detailStartCellIndex = parseInt(tableDetailsStartInfo[0], 10);
    const detailStartRowIndex = parseInt(tableDetailsStartInfo[1], 10);
    let dataRowIndex = detailStartRowIndex + 1;
    const detailStyle = worksheet.getRow(detailStartRowIndex).getCell(detailStartCellIndex).style;
    const totalStyle = worksheet.getRow(detailStartRowIndex).getCell(4).style;
    for (const detailsMap of taxMap.entries()) {
        let total = 0;
        let columnIndex = 2;
        const tax = detailsMap[0];
        worksheet.getRow(dataRowIndex).getCell(columnIndex).value = tax;
        worksheet.getRow(dataRowIndex).getCell(columnIndex).style = detailStyle;
        for (const date of headers) {
            ++columnIndex;
            if (detailsMap[1].has(date.dayOfDate)) {
                worksheet.getRow(dataRowIndex).getCell(columnIndex).value = detailsMap[1].get(date.dayOfDate);
                worksheet.getRow(dataRowIndex).getCell(columnIndex).style = detailStyle;
            }
            else {
                worksheet.getRow(dataRowIndex).getCell(columnIndex).value = 0;
                worksheet.getRow(dataRowIndex).getCell(columnIndex).style = detailStyle;
            }
            const value = worksheet.getRow(dataRowIndex).getCell(columnIndex).value;
            total = total + (value ? parseFloat(value.toString()) : 0);
        }
        worksheet.getRow(dataRowIndex).getCell(++columnIndex).value = total;
        worksheet.getRow(dataRowIndex).getCell(columnIndex).style = totalStyle;
        if (taxTotalPrevAmtMap.has(tax)) {
            if (taxTotalPrevAmtMap.get(tax)) {
                worksheet.getRow(dataRowIndex).getCell(++columnIndex).value = taxTotalPrevAmtMap.get(tax);
            }
            else {
                worksheet.getRow(dataRowIndex).getCell(++columnIndex).value = 0;
            }
        }
        else {
            worksheet.getRow(dataRowIndex).getCell(++columnIndex).value = 0;
        }
        worksheet.getRow(dataRowIndex).getCell(columnIndex).style = totalStyle;
        const previousMonthTotal = worksheet.getRow(dataRowIndex).getCell(columnIndex).value;
        const grandTotal = total + (previousMonthTotal ? parseFloat(previousMonthTotal.toString()) : 0);
        worksheet.getRow(dataRowIndex).getCell(++columnIndex).value = grandTotal;
        worksheet.getRow(dataRowIndex).getCell(columnIndex).style = totalStyle;
        dataRowIndex++;
    }
    // Previous Month taxes which are not having for Current Month
    await processPrevMonthTaxes(taxMap, taxTotalPrevAmtMap, worksheet, dataRowIndex, detailStyle, headers);
    await createSummaryRow(taxMap, taxTotalPrevAmtMap, worksheet, dataRowIndex, totalStyle, headers);
};
const processPrevMonthTaxes = async (taxMap, taxTotalPrevAmtMap, worksheet, dataRowIndex, detailStyle, headers) => {
    if (taxMap && taxMap.size > 0) {
        for (const entry of taxMap.entries()) {
            taxTotalPrevAmtMap.delete(entry[0]);
        }
    }
    for (const map of taxTotalPrevAmtMap.entries()) {
        let col = 2;
        worksheet.getRow(++dataRowIndex).getCell(col).value = map[0];
        worksheet.getRow(dataRowIndex).getCell(col).style = detailStyle;
        for (const dates of headers) {
            worksheet.getRow(dataRowIndex).getCell(++col).value = 0;
            worksheet.getRow(dataRowIndex).getCell(col).style = detailStyle;
        }
        worksheet.getRow(dataRowIndex).getCell(++col).value = 0;
        worksheet.getRow(dataRowIndex).getCell(col).style = detailStyle;
        worksheet.getRow(dataRowIndex).getCell(++col).value = map[1];
        worksheet.getRow(dataRowIndex).getCell(col).style = detailStyle;
        worksheet.getRow(dataRowIndex).getCell(++col).value = map[1];
        worksheet.getRow(dataRowIndex).getCell(col).style = detailStyle;
    }
};
const createSummaryRow = async (taxMap, taxTotalPrevAmtMap, worksheet, dataRowIndex, totalStyle, headers) => {
    if (taxMap.size > 0 || taxTotalPrevAmtMap.size > 0) {
        // Grand Total Row In Sheet
        worksheet.getRow(dataRowIndex).getCell(2).value = 'કૂલ (Total)';
        worksheet.getRow(dataRowIndex).getCell(2).style = totalStyle;
        for (let i = 3; i < headers.length + 6; i++) {
            let grandTotal = 0;
            for (let j = 12; j < dataRowIndex; j++) {
                const amount = worksheet.getRow(j).getCell(i).value;
                grandTotal = grandTotal + (amount ? parseFloat(amount.toString()) : 0);
            }
            worksheet.getRow(dataRowIndex).getCell(i).value = grandTotal;
            worksheet.getRow(dataRowIndex).getCell(i).style = totalStyle;
        }
    }
};
const createMapToProcessDetailsData = async (form6DetailsMapList, taxMap, date, header) => {
    for (const form6DetailsMap of form6DetailsMapList) {
        const taxDetail = form6DetailsMap.HEAD_NAME;
        const taxAmt = form6DetailsMap.INCOME_AMOUNT;
        if (taxMap.has(taxDetail)) {
            if (taxAmt) {
                taxMap.get(taxDetail).set(date, taxAmt);
            }
            else {
                taxMap.get(taxDetail).set(date, 0);
            }
        }
        else {
            const dateAmountMap = new Map();
            taxMap.set(taxDetail, dateAmountMap);
            if (taxAmt) {
                taxMap.get(taxDetail).set(header.dayOfDate, taxAmt);
            }
            else {
                taxMap.get(taxDetail).set(header.dayOfDate, 0);
            }
        }
    }
};
const fetchData = async (query, reportOptions) => {
    if (query !== constants_1.NA) {
        const response = await services_1.queryService.executeNativeQuery(query, reportOptions);
        return utils_1.formatResponse(response);
    }
    return [];
};
const form6Builder = new Form6Builder();
exports.default = form6Builder;
//# sourceMappingURL=MonthlyIncomeReport(Form6).js.map