import Excel from 'exceljs';
import { WorkSheetReport } from '../metadata/models';
import { ReportOptions } from '../models';
declare class Form9Builder {
    build(metadata: WorkSheetReport, worksheet: Excel.Worksheet, reportOptions: ReportOptions): Promise<void>;
}
declare const form9Builder: Form9Builder;
export default form9Builder;
//# sourceMappingURL=FinancialYearWise_TaxCollection.d.ts.map