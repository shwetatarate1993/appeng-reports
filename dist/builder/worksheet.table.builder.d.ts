import Excel from 'exceljs';
import { WorkSheetReport } from '../metadata/models';
import { ReportOptions } from '../models';
declare class WorksheetTableBuilder {
    build(metadata: WorkSheetReport, worksheet: Excel.Worksheet, reportOptions: ReportOptions): Promise<void>;
}
declare const builder: WorksheetTableBuilder;
export default builder;
//# sourceMappingURL=worksheet.table.builder.d.ts.map