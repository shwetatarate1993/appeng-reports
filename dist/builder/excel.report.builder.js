"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const exceljs_1 = require("exceljs");
const info_commons_1 = require("info-commons");
const constants_1 = require("../constants");
const excel_worksheet_builder_1 = __importDefault(require("./excel.worksheet.builder"));
class ExcelReportBuilder {
    async build(reportMD, reportOptions) {
        let language = 'en';
        if (reportOptions.language) {
            language = reportOptions.language;
        }
        const workBook = new exceljs_1.Workbook();
        if (info_commons_1.APP_ENV === 'desktop') {
            const template = './templates/' + reportOptions.templateName + '_' + language + '.xlsx';
            const response = await fetch(new Request(template));
            const arrayBuffer = await response.arrayBuffer();
            await workBook.xlsx.load(arrayBuffer);
        }
        else {
            const template = constants_1.getTemplatePath() + reportOptions.templateName + '_' + language + '.xlsx';
            await workBook.xlsx.readFile(template);
        }
        for (const worksheetMD of reportMD.excelWorkSheets) {
            await excel_worksheet_builder_1.default.build(worksheetMD, workBook, reportOptions);
        }
        workBook.removeWorksheet(constants_1.METADATA_SHEETNAME);
        // await workBook.xlsx.writeFile(reportMD.name + '.xlsx');
        return await workBook.xlsx.writeBuffer();
    }
}
exports.ExcelReportBuilder = ExcelReportBuilder;
//# sourceMappingURL=excel.report.builder.js.map