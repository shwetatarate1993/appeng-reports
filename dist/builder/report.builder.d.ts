import { AEReport } from '../metadata/models';
import { ReportOptions } from '../models';
export default interface ReportBuilder {
    build(reportMedata: AEReport, reportOptions: ReportOptions): Promise<any>;
}
//# sourceMappingURL=report.builder.d.ts.map