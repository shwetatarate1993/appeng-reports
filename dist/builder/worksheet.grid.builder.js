"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const services_1 = require("../services");
const services_2 = require("../services");
const utils_1 = require("../utils");
class WorksheetGridBuilder {
    async build(metadata, worksheet, reportOptions) {
        const dataRecord = await getData(metadata, reportOptions);
        const data = { rowData: dataRecord, params: reportOptions.params };
        const gridStartEndInfo = metadata.location.split(constants_1.COLON);
        const startingCell = gridStartEndInfo[0].split(constants_1.COMMA);
        const endingCell = gridStartEndInfo[1].split(constants_1.COMMA);
        const startCellIndex = parseInt(startingCell[0], 10);
        const startRowIndex = parseInt(startingCell[1], 10);
        const endCellIndex = parseInt(endingCell[0], 10);
        const endRowIndex = parseInt(endingCell[1], 10);
        for (let rowIndex = startRowIndex; rowIndex <= endRowIndex; rowIndex++) {
            for (let cellIndex = startCellIndex; cellIndex <= endCellIndex; cellIndex++) {
                const cell = worksheet.getRow(rowIndex).getCell(cellIndex);
                if (cell && cell.value) {
                    let value = cell.value.toString();
                    value = await getProcessedValue(value, data);
                    worksheet.getRow(rowIndex).getCell(cellIndex).value = value;
                    worksheet.getRow(rowIndex).getCell(cellIndex).style = cell.style;
                }
            }
        }
    }
}
const getData = async (metadata, reportOptions) => {
    let dataRecord = {};
    if (metadata.data.reportQueries[0].query !== constants_1.NA) {
        const dataRecords = await fetchData(metadata.data.reportQueries[0].query, reportOptions);
        if (dataRecords.length > 0) {
            dataRecord = dataRecords[0];
        }
    }
    return dataRecord;
};
const getProcessedValue = async (value, data) => {
    switch (value.substring(2, value.indexOf(constants_1.HASH, 2))) {
        case constants_1.DS:
            const columnName = value.substring(value.indexOf('${') + 2, value.indexOf('}#')).trim();
            value = data.rowData[columnName] ? data.rowData[columnName] : '';
            break;
        case constants_1.CellValueType.GENERIC_EXPRESSION:
            value = services_1.expressionResolver.executeGenericExpressions(data, value);
            break;
        case constants_1.CellValueType.NUMERIC_EXPRESSION:
            value = services_1.expressionResolver.executeNumericExpressions(data, value);
            break;
        case constants_1.CellValueType.INPUT:
            value = services_1.expressionResolver.resolveInputPlaceHoder(data.params, value);
            break;
    }
    return value;
};
const fetchData = async (query, reportOptions) => {
    const response = await services_2.queryService.executeNativeQuery(query, reportOptions);
    return utils_1.formatResponse(response);
};
const builder = new WorksheetGridBuilder();
exports.default = builder;
//# sourceMappingURL=worksheet.grid.builder.js.map