import { ExcelReport } from '../metadata/models';
import { ReportOptions } from '../models';
import ReportBuilder from './report.builder';
export declare class ExcelReportBuilder implements ReportBuilder {
    build(reportMD: ExcelReport, reportOptions: ReportOptions): Promise<any>;
}
//# sourceMappingURL=excel.report.builder.d.ts.map