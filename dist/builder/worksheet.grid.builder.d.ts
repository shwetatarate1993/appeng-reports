import Excel from 'exceljs';
import { WorkSheetReport } from '../metadata/models';
import { ReportOptions } from '../models';
declare class WorksheetGridBuilder {
    build(metadata: WorkSheetReport, worksheet: Excel.Worksheet, reportOptions: ReportOptions): Promise<void>;
}
declare const builder: WorksheetGridBuilder;
export default builder;
//# sourceMappingURL=worksheet.grid.builder.d.ts.map