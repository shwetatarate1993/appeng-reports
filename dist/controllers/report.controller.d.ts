import { Request, Response } from 'express';
declare class ReportController {
    download(request: Request, response: Response): Promise<Response>;
}
declare const _default: ReportController;
export default _default;
//# sourceMappingURL=report.controller.d.ts.map