"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const generators_1 = require("../metadata/generators");
const models_1 = require("../models");
class ReportController {
    async download(request, response) {
        const errorResponse = await info_commons_1.validateToken(request.get(info_commons_1.TOKEN_KEY), request.get(info_commons_1.PROVIDER));
        if (errorResponse !== null) {
            return response.status(errorResponse.code).json(errorResponse);
        }
        const reportOptions = new models_1.ReportOptions(request.body.templateName, request.get(info_commons_1.TOKEN_KEY), request.body.params, request.body.language);
        const excelReport = await generators_1.metadataGenerator.createReportMetadata(reportOptions);
        const excelBytes = await generators_1.metadataGenerator.buildReport(excelReport, reportOptions);
        response.status(200).json(excelBytes);
    }
}
exports.default = new ReportController();
//# sourceMappingURL=report.controller.js.map