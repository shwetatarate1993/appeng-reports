import { Aggregation, CellValueType } from '../constants';
export default class Summary {
    readonly cellValueType: CellValueType;
    readonly summaryType: Aggregation;
    value: any;
    internalTempData: number;
    readonly summaryExpression: string;
    constructor(cellValueType: CellValueType, value: any, summaryExpression?: string, summaryType?: Aggregation);
}
//# sourceMappingURL=summary.model.d.ts.map