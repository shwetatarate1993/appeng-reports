"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ReportOptions {
    constructor(templateName, jwt, params, language) {
        this.templateName = templateName;
        this.jwt = jwt;
        this.params = params;
        this.language = language;
    }
}
exports.default = ReportOptions;
//# sourceMappingURL=report.options.model.js.map