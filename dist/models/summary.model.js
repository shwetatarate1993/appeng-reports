"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Summary {
    constructor(cellValueType, value, summaryExpression, summaryType) {
        this.internalTempData = 0;
        this.cellValueType = cellValueType;
        this.value = value;
        this.summaryExpression = summaryExpression;
        this.summaryType = summaryType;
    }
}
exports.default = Summary;
//# sourceMappingURL=summary.model.js.map