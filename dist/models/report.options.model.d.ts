export default class ReportOptions {
    templateName: string;
    language: string;
    jwt: string;
    params: any;
    constructor(templateName: string, jwt: string, params: any, language?: string);
}
//# sourceMappingURL=report.options.model.d.ts.map