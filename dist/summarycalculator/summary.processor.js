"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const services_1 = require("../services");
class SummaryProcessor {
    resolveSummaryExpr(summary, data, rowIndex) {
        let value;
        const expr = summary.summaryExpression;
        switch (expr.substring(2, expr.indexOf(constants_1.HASH, 2)).trim()) {
            case constants_1.DS:
                const columnName = expr.substring(expr.indexOf('${') + 2, expr.indexOf('}')).trim();
                value = data.dataRow[columnName] ? data.dataRow[columnName] : 0;
                break;
            case constants_1.CellValueType.GENERIC_EXPRESSION:
                value = services_1.expressionResolver.executeGenericExpressions(data, expr);
                break;
            case constants_1.CellValueType.NUMERIC_EXPRESSION:
                value = services_1.expressionResolver.executeNumericExpressions(data, expr);
                break;
            case constants_1.CellValueType.INPUT:
                value = services_1.expressionResolver.resolveInputPlaceHoder(data.params, expr);
                break;
        }
        return value;
    }
}
const summaryProcessor = new SummaryProcessor();
Object.freeze(summaryProcessor);
exports.default = summaryProcessor;
//# sourceMappingURL=summary.processor.js.map