"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
class AverageCalculator {
    calculate(summary, data, rowIndex) {
        const value = _1.summaryProcessor.resolveSummaryExpr(summary, data, rowIndex);
        summary.internalTempData = summary.internalTempData + value;
        summary.value = summary.internalTempData / ++rowIndex;
    }
}
const averageCalculator = new AverageCalculator();
Object.freeze(averageCalculator);
exports.default = averageCalculator;
//# sourceMappingURL=average.calculator.js.map