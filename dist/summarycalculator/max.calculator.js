"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
class MaximumCalculator {
    calculate(summary, data, rowIndex) {
        const value = _1.summaryProcessor.resolveSummaryExpr(summary, data, rowIndex);
        if (rowIndex + 1 === 1) {
            summary.value = value;
        }
        if (value > summary.value) {
            summary.value = value;
        }
    }
}
const maxCalculator = new MaximumCalculator();
Object.freeze(maxCalculator);
exports.default = maxCalculator;
//# sourceMappingURL=max.calculator.js.map