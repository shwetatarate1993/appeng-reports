import { Summary } from '../models';
import { AggregationCalculator } from './';
declare class MinimumCalculator implements AggregationCalculator {
    calculate(summary: Summary, data: any, rowIndex: number): any;
}
declare const minCalculator: MinimumCalculator;
export default minCalculator;
//# sourceMappingURL=min.calculator.d.ts.map