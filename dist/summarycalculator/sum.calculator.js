"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
class SumCalculator {
    calculate(summary, data, rowIndex) {
        const value = _1.summaryProcessor.resolveSummaryExpr(summary, data, rowIndex);
        summary.value = summary.value + value;
    }
}
const sumCalculator = new SumCalculator();
Object.freeze(sumCalculator);
exports.default = sumCalculator;
//# sourceMappingURL=sum.calculator.js.map