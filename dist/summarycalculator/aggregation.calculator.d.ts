import { Summary } from '../models';
export default interface AggregationCalculator {
    calculate(summary: Summary, data: any, rowIndex: number): any;
}
//# sourceMappingURL=aggregation.calculator.d.ts.map