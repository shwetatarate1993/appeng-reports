import { Summary } from '../models';
import AggregationCalculator from './aggregation.calculator';
declare class AverageCalculator implements AggregationCalculator {
    calculate(summary: Summary, data: any, rowIndex: number): void;
}
declare const averageCalculator: AverageCalculator;
export default averageCalculator;
//# sourceMappingURL=average.calculator.d.ts.map