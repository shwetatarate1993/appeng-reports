import { Summary } from '../models';
import { AggregationCalculator } from './';
declare class MaximumCalculator implements AggregationCalculator {
    calculate(summary: Summary, data: any, rowIndex: number): any;
}
declare const maxCalculator: MaximumCalculator;
export default maxCalculator;
//# sourceMappingURL=max.calculator.d.ts.map