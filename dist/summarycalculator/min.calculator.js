"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
class MinimumCalculator {
    calculate(summary, data, rowIndex) {
        const value = _1.summaryProcessor.resolveSummaryExpr(summary, data, rowIndex);
        if (rowIndex + 1 === 1) {
            summary.value = value;
        }
        if (value < summary.value) {
            summary.value = value;
        }
    }
}
const minCalculator = new MinimumCalculator();
Object.freeze(minCalculator);
exports.default = minCalculator;
//# sourceMappingURL=min.calculator.js.map