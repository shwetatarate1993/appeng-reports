"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var average_calculator_1 = require("./average.calculator");
exports.averageCalculator = average_calculator_1.default;
var min_calculator_1 = require("./min.calculator");
exports.minCalculator = min_calculator_1.default;
var max_calculator_1 = require("./max.calculator");
exports.maxCalculator = max_calculator_1.default;
var sum_calculator_1 = require("./sum.calculator");
exports.sumCalculator = sum_calculator_1.default;
var summary_processor_1 = require("./summary.processor");
exports.summaryProcessor = summary_processor_1.default;
//# sourceMappingURL=index.js.map