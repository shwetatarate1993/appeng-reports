import { Summary } from '../models';
import AggregationCalculator from './aggregation.calculator';
declare class SumCalculator implements AggregationCalculator {
    calculate(summary: Summary, data: any, rowIndex: number): void;
}
declare const sumCalculator: SumCalculator;
export default sumCalculator;
//# sourceMappingURL=sum.calculator.d.ts.map