import { Summary } from '../models';
declare class SummaryProcessor {
    resolveSummaryExpr(summary: Summary, data: any, rowIndex: number): any;
}
declare const summaryProcessor: SummaryProcessor;
export default summaryProcessor;
//# sourceMappingURL=summary.processor.d.ts.map