"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const convict_1 = __importDefault(require("convict"));
// Define a schema
const config = convict_1.default({
    db: {
        PRIMARYSPRING: {
            mysql: {
                dialect: 'mysql',
                host: {
                    doc: 'Database host name/IP',
                    format: '*',
                    default: 'awsmysqldev1.ckb0c9p1c3xw.us-east-1.rds.amazonaws.com',
                },
                name: {
                    doc: 'Database name',
                    format: String,
                    default: 'villageportalappdev',
                },
                username: {
                    doc: 'Database Username',
                    format: String,
                    default: 'awsdevmaster',
                },
                password: {
                    doc: 'Database Password',
                    format: String,
                    default: 'awsinfo2018',
                },
                pool: {
                    max: 5,
                    min: 0,
                    acquire: 30000,
                    idle: 10000,
                },
            },
            sqlite3: {
                dialect: 'sqlite3',
                connection: {
                    filename: 'F:/config/villageportaldb/villageportalapp.db',
                },
            },
        },
    },
});
// Perform validation
config.validate({ allowed: 'strict' });
exports.default = config;
//# sourceMappingURL=index.js.map