"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const formatResponse = (response) => {
    if (response.data && response.data.columns && response.data.rows) {
        const columns = response.data.columns;
        const records = [];
        response.data.rows.map((row) => {
            const record = {};
            for (const [i, value] of row.entries()) {
                record[columns[i]] = value;
            }
            records.push(record);
        });
        return records;
    }
    return null;
};
exports.default = formatResponse;
//# sourceMappingURL=response.formatter.utils.js.map