"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mathjs_1 = require("mathjs");
const moment_1 = __importDefault(require("moment"));
exports.createFunction = (expression) => {
    const strFunction = ' var evaluate = dependecies.evaluate;\n'
        + ' var moment = dependecies.moment; \n'
        + ' return ' + expression + ';';
    const dynamicFunction = new Function('dependecies', 'input', strFunction);
    return dynamicFunction;
};
exports.executeGenericExpression = (input, expression) => {
    const dependecies = {
        evaluate: mathjs_1.evaluate,
        moment: moment_1.default,
    };
    const exprFunction = exports.createFunction(expression);
    return exprFunction(dependecies, input);
};
exports.executeNumericExpression = (input, expression) => {
    const expParser = mathjs_1.parser();
    expParser.set('input', input);
    return expParser.evaluate(expression);
};
exports.formatDate = (date, dateFormat) => {
    // todo
    // use moment package
};
exports.formatDateForCountry = (date, countryCode) => {
    // todo
    // use country-language and Intl package
};
exports.formatCurrencyforCountry = (currency, countryCode) => {
    // todo
    // use country-language and Intl package
};
exports.formatNumberForCountry = (input, countryCode) => {
    // todo
    // use country-language and Intl package
};
//# sourceMappingURL=expression.function.js.map