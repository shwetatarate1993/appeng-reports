export declare const createFunction: (expression: string) => Function;
export declare const executeGenericExpression: (input: any, expression: string) => any;
export declare const executeNumericExpression: (input: any, expression: string) => any;
export declare const formatDate: (date: any, dateFormat: string) => void;
export declare const formatDateForCountry: (date: any, countryCode: string) => void;
export declare const formatCurrencyforCountry: (currency: string, countryCode: string) => void;
export declare const formatNumberForCountry: (input: number, countryCode: string) => void;
//# sourceMappingURL=expression.function.d.ts.map