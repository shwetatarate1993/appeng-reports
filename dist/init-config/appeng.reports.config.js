"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_query_1 = require("info-query");
class AppengReportsConfig {
    static configure(config, dialect) {
        info_query_1.InfoQueryConfig.configure(config, dialect);
    }
}
exports.default = AppengReportsConfig;
//# sourceMappingURL=appeng.reports.config.js.map