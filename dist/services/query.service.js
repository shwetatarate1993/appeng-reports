"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const info_query_1 = require("info-query");
class QueryService {
    async executeNativeQuery(query, reportOptions) {
        query = info_commons_1.queryResolver(info_commons_1.queryParser(info_commons_1.queryExtractor(query, info_commons_1.DIALECT), reportOptions.params), reportOptions.params);
        const nativeQuery = {
            database: '11111111-1111-1111-1111-111111111111',
            native: {
                query,
            },
            type: 'native',
            isNative: true,
            datasource: 'PRIMARYSPRING',
        };
        const queryMetadata = new info_query_1.QueryMetadata(nativeQuery);
        await queryMetadata.init(reportOptions.jwt);
        const queryResponse = await info_query_1.queryExecutionService.executeQuery(queryMetadata, reportOptions.jwt);
        return queryResponse;
    }
}
const queryService = new QueryService();
Object.freeze(queryService);
exports.default = queryService;
//# sourceMappingURL=query.service.js.map