"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MetadataService {
    constructor() {
        this.excelReports = [];
        /** No Operation */
    }
    static get INSTANCE() {
        if (this.metadataService === undefined || this.metadataService === null) {
            return this.metadataService = new MetadataService();
        }
        return this.metadataService;
    }
    get excelReportList() {
        return this.excelReports;
    }
}
exports.default = MetadataService;
//# sourceMappingURL=metadata.service.js.map