import { ReportOptions } from '../models';
declare class QueryService {
    executeNativeQuery(query: string, reportOptions: ReportOptions): Promise<any>;
}
declare const queryService: QueryService;
export default queryService;
//# sourceMappingURL=query.service.d.ts.map