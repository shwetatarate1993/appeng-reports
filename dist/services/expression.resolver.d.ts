declare class ExpressionResolver {
    executeGenericExpressions(input: any, cellValue: string): string;
    executeNumericExpressions(input: any, cellValue: string): string;
    resolveInputPlaceHoder(input: any, cellValue: string): string;
}
declare const expressionResolver: ExpressionResolver;
export default expressionResolver;
//# sourceMappingURL=expression.resolver.d.ts.map