import { ExcelReport } from '../metadata/models';
export default class MetadataService {
    private static metadataService;
    private excelReports;
    private constructor();
    static readonly INSTANCE: MetadataService;
    readonly excelReportList: ExcelReport[];
}
//# sourceMappingURL=metadata.service.d.ts.map