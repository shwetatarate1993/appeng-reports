"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const expression_function_1 = require("../utils/expression.function");
class ExpressionResolver {
    executeGenericExpressions(input, cellValue) {
        cellValue = cellValue.substring(cellValue.indexOf(constants_1.GENERIC_PLACEHOLDER) + 10).trim();
        const value = expression_function_1.executeGenericExpression(input, cellValue);
        return cellValue !== value && !value.includes(undefined) ? value : '';
    }
    executeNumericExpressions(input, cellValue) {
        cellValue = cellValue.substring(cellValue.indexOf(constants_1.NUMERIC_PLACEHOLDER) + 10).trim();
        const value = expression_function_1.executeNumericExpression(input, cellValue);
        return cellValue !== value ? value : '';
    }
    resolveInputPlaceHoder(input, cellValue) {
        cellValue = cellValue.substring(cellValue.indexOf(constants_1.INPUT_PLACEHOLDER) + 8).trim();
        const value = input[cellValue];
        return value && cellValue !== value ? value : '';
    }
}
const expressionResolver = new ExpressionResolver();
exports.default = expressionResolver;
//# sourceMappingURL=expression.resolver.js.map