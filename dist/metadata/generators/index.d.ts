export { default as metadataGenerator } from './metadata.generator';
import { ExcelReportBuilder } from '../../builder/excel.report.builder';
export declare const getReportBuilderInstance: (fileType: string) => ExcelReportBuilder;
//# sourceMappingURL=index.d.ts.map