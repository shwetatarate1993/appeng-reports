import { ReportOptions } from '../../models';
import { ExcelReport } from '../models';
declare class MetadataGenerator {
    createReportMetadata(reportOptions: ReportOptions): Promise<ExcelReport>;
    buildReport(excelReportMetadata: ExcelReport, reportOptions: ReportOptions): Promise<any>;
}
declare const metadataGenerator: MetadataGenerator;
export default metadataGenerator;
//# sourceMappingURL=metadata.generator.d.ts.map