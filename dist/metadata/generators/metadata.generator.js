"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const exceljs_1 = require("exceljs");
const info_commons_1 = require("info-commons");
const constants_1 = require("../../constants");
const services_1 = require("../../services");
const models_1 = require("../models");
const _1 = require("./");
class MetadataGenerator {
    async createReportMetadata(reportOptions) {
        let language = 'en';
        if (reportOptions.language) {
            language = reportOptions.language;
        }
        const workBook = new exceljs_1.Workbook();
        let template;
        if (info_commons_1.APP_ENV === 'desktop') {
            template = './templates/' + reportOptions.templateName + '_' + language + '.xlsx';
            const response = await fetch(new Request(template));
            const arrayBuffer = await response.arrayBuffer();
            await workBook.xlsx.load(arrayBuffer);
        }
        else {
            template = constants_1.getTemplatePath() + reportOptions.templateName + '_' + language + '.xlsx';
            await workBook.xlsx.readFile(template);
        }
        const workSheet = workBook.getWorksheet(constants_1.METADATA_SHEETNAME);
        return await createExcelReportMetadata(workBook, workSheet, template);
    }
    async buildReport(excelReportMetadata, reportOptions) {
        const metaDataService = services_1.MetadataService.INSTANCE;
        metaDataService.excelReportList.push(excelReportMetadata);
        const reportBuilder = _1.getReportBuilderInstance(excelReportMetadata.fileType);
        const excelBytes = await reportBuilder.build(excelReportMetadata, reportOptions);
        return excelBytes;
    }
}
const createExcelReportMetadata = async (workBook, workSheet, template) => {
    const excelWorkSheets = await createExcelWorkSheets(workBook, workSheet);
    const reportName = workSheet.getCell('B2').value.toString();
    const excelReport = new models_1.ExcelReport(reportName, template, excelWorkSheets);
    return excelReport;
};
const createExcelWorkSheets = async (workBook, workSheet) => {
    let workSheetReports = [];
    const excelWorkSheetsMap = new Map();
    for (let mdRowIndex = 2; mdRowIndex <= workSheet.rowCount; mdRowIndex++) {
        const row = workSheet.getRow(mdRowIndex);
        const workSheetReport = await createWorkSheetReport(row);
        const sheetName = row.getCell(1).value.toString();
        if (excelWorkSheetsMap.get(sheetName) === undefined) {
            workSheetReports = [];
            workSheetReports.push(workSheetReport);
            const excelWorkSheet = new models_1.ExcelWorkSheet(sheetName, workBook.getWorksheet(sheetName).id, workSheetReports);
            excelWorkSheetsMap.set(sheetName, excelWorkSheet);
        }
        else {
            excelWorkSheetsMap.get(sheetName).workSheetReports.push(workSheetReport);
        }
    }
    return getExcelWorkSheets(excelWorkSheetsMap);
};
const getExcelWorkSheets = (excelWorkSheetsMap) => {
    const excelWorkSheets = [];
    excelWorkSheetsMap.forEach((value, key, map) => {
        excelWorkSheets.push(value);
    });
    return excelWorkSheets;
};
const createWorkSheetReport = async (row) => {
    const workSheetReport = new models_1.WorkSheetReport(row.getCell(5).value.toString(), row.getCell(3).value.toString(), row.getCell(4).value.toString(), row.getCell(6).value.toString(), await createHeader(row), await createData(row), await createSummary(row));
    return workSheetReport;
};
const createHeader = async (row) => {
    const reportQuery = new models_1.ReportQuery(row.getCell(12).value.toString(), row.getCell(9).value.toString(), row.getCell(10).value.toString());
    const reportQueries = [reportQuery];
    const header = new models_1.WorkSheetReportHeader(row.getCell(7).value.toString(), row.getCell(8).value.toString(), reportQueries);
    return header;
};
const createData = async (row) => {
    const reportQuery = new models_1.ReportQuery(row.getCell(12).value.toString(), row.getCell(13).value.toString(), row.getCell(14).value.toString());
    const reportQueries = [reportQuery];
    const data = new models_1.Data(row.getCell(11).value.toString(), reportQueries);
    return data;
};
const createSummary = async (row) => {
    const summary = new models_1.Summary(row.getCell(15).value.toString(), row.getCell(16).value.toString(), row.getCell(17).value.toString());
    return summary;
};
const metadataGenerator = new MetadataGenerator();
Object.freeze(metadataGenerator);
exports.default = metadataGenerator;
//# sourceMappingURL=metadata.generator.js.map