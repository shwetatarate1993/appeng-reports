"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var metadata_generator_1 = require("./metadata.generator");
exports.metadataGenerator = metadata_generator_1.default;
const excel_report_builder_1 = require("../../builder/excel.report.builder");
const constants_1 = require("../../constants");
exports.getReportBuilderInstance = (fileType) => {
    switch (fileType) {
        case constants_1.FileType.EXCEL:
            return new excel_report_builder_1.ExcelReportBuilder();
        default:
            throw new Error(fileType + ' file not supported');
    }
};
//# sourceMappingURL=index.js.map