import { WorkSheetReport } from './';
export default class ExcelWorkSheet {
    readonly name: string;
    readonly index: number;
    readonly workSheetReports: WorkSheetReport[];
    constructor(name: string, index: number, workSheetReports: WorkSheetReport[]);
}
//# sourceMappingURL=excel.worksheet.d.ts.map