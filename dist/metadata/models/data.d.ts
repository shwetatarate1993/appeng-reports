import { ReportQuery } from './';
export default class Data {
    location: string;
    reportQueries: ReportQuery[];
    constructor(location: string, reportQueries: ReportQuery[]);
}
//# sourceMappingURL=data.d.ts.map