import { ExcelWorkSheet } from './';
import { AEReport } from './';
export default class ExcelReport extends AEReport {
    readonly name: string;
    readonly templatePath: string;
    readonly excelWorkSheets: ExcelWorkSheet[];
    constructor(name: string, templatePath: string, excelWorkSheets: ExcelWorkSheet[]);
}
//# sourceMappingURL=excel.report.d.ts.map