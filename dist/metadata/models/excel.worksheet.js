"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ExcelWorkSheet {
    constructor(name, index, workSheetReports) {
        this.name = name;
        this.index = index;
        this.workSheetReports = workSheetReports;
    }
}
exports.default = ExcelWorkSheet;
//# sourceMappingURL=excel.worksheet.js.map