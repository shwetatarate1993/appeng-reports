"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ReportQuery {
    constructor(dataSource, dataSourceType, query) {
        this.dataSource = dataSource;
        this.dataSourceType = dataSourceType;
        this.query = query;
    }
}
exports.default = ReportQuery;
//# sourceMappingURL=report.query.js.map