import { Data, Summary, WorkSheetReportHeader } from './';
export default class WorkSheetReport {
    readonly type: string;
    readonly locationType: string;
    readonly location: string;
    readonly inputs: any;
    readonly header: WorkSheetReportHeader;
    readonly data: Data;
    readonly summary: Summary;
    constructor(type: string, locationType: string, location: string, inputs: any, header: WorkSheetReportHeader, data: Data, summary: Summary);
}
//# sourceMappingURL=worksheet.report.d.ts.map