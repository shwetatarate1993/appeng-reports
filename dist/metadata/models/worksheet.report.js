"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class WorkSheetReport {
    constructor(type, locationType, location, inputs, header, data, summary) {
        this.type = type;
        this.locationType = locationType;
        this.location = location;
        this.inputs = inputs;
        this.header = header;
        this.data = data;
        this.summary = summary;
    }
}
exports.default = WorkSheetReport;
//# sourceMappingURL=worksheet.report.js.map