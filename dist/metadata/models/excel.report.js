"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../constants");
const _1 = require("./");
class ExcelReport extends _1.AEReport {
    constructor(name, templatePath, excelWorkSheets) {
        super();
        this.fileType = constants_1.FileType.EXCEL;
        this.name = name;
        this.templatePath = templatePath;
        this.excelWorkSheets = excelWorkSheets;
    }
}
exports.default = ExcelReport;
//# sourceMappingURL=excel.report.js.map