"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Summary {
    constructor(location, type, level) {
        this.location = location;
        this.type = type;
        this.level = level;
    }
}
exports.default = Summary;
//# sourceMappingURL=summary.js.map