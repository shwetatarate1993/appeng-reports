"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class WorkSheetReportHeader {
    constructor(type, location, reportQueries) {
        this.type = type;
        this.location = location;
        this.reportQueries = reportQueries;
    }
}
exports.default = WorkSheetReportHeader;
//# sourceMappingURL=worksheet.report.header.js.map