export default class ReportQuery {
    readonly dataSource: string;
    readonly dataSourceType: string;
    readonly query: string;
    constructor(dataSource: string, dataSourceType: string, query: string);
}
//# sourceMappingURL=report.query.d.ts.map