import { ReportQuery } from './';
export default class WorkSheetReportHeader {
    readonly type: string;
    readonly location: string;
    readonly reportQueries: ReportQuery[];
    constructor(type: string, location: string, reportQueries: ReportQuery[]);
}
//# sourceMappingURL=worksheet.report.header.d.ts.map