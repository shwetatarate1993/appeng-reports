declare const _default: {
    'Serial No': number;
    'State': string;
    'District': string;
    'Taluka': string;
    'Grampanchayat': string;
    'Distributed Grant': number;
    'Expense of last month of current year': number;
    'Expense of current month of current year': number;
    'Total expense of current year': number;
    'Achievement of last month of current year': number;
    'Achievement of current month of current year': number;
    'Total Achievement of current year': number;
    'Created On': string;
}[];
export default _default;
//# sourceMappingURL=cdp-14.d.ts.map