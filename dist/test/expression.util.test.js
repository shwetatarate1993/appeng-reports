"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const expression_function_1 = require("../utils/expression.function");
test('Generic expression ', () => {
    const val = expression_function_1.executeGenericExpression({}, '\'Report Date : \'+ moment().format(\'dddd, MMMM Do YYYY, h:mm:ss a\')');
    expect(val).toContain('Report Date : ');
});
test('Nested Json Generic expression ', () => {
    const inputParams = { GRAMPANCHAYAT: 'Shahibaug', MONTH: 9, YEAR: 2019 };
    const data = { rowData: {}, params: inputParams };
    // tslint:disable-next-line: quotemark
    const expression = "'મોજે. '+input.params.GRAMPANCHAYAT+' ગ્રામપંચાયત'";
    const val = expression_function_1.executeGenericExpression(data, expression);
    expect(val).toContain('મોજે. Shahibaug ગ્રામપંચાયત');
});
test('Single Json Generic expression ', () => {
    const val = expression_function_1.executeGenericExpression({ firstName: 'Ankur', lastName: 'Gupta' }, '\'NAME: \'+input.firstName+\' \'+input.lastName');
    expect(val).toEqual('NAME: Ankur Gupta');
});
test('Numeric add expression ', () => {
    const val = expression_function_1.executeNumericExpression({ a: 2 }, 'input.a*4');
    expect(val).toBe(8);
});
//# sourceMappingURL=expression.util.test.js.map