"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const excel_report_builder_1 = require("../builder/excel.report.builder");
const constants = __importStar(require("../constants"));
const generators_1 = require("../metadata/generators");
const models_1 = require("../metadata/models");
const models_2 = require("../models");
const services_1 = require("../services");
test('createReportMetadata() test', async () => {
    const path = './packages/appeng-reports/dist/templates/';
    const templatePathMock = sinon_1.default.mock(constants);
    templatePathMock.expects('getTemplatePath').atLeast(1).returns(path);
    const reportOptions = new models_2.ReportOptions('CDP-14', '', {}, 'en');
    const excelReport = await generators_1.metadataGenerator.createReportMetadata(reportOptions);
    expect(excelReport.name).toEqual('CDP-14');
    expect(excelReport.templatePath).toEqual(path + 'CDP-14_en.xlsx');
    expect(excelReport.excelWorkSheets[0].name).toEqual('CDP-14');
    expect(excelReport.excelWorkSheets[0].index).toEqual(2);
    const workSheetReport = excelReport.excelWorkSheets[0].workSheetReports[0];
    expect(workSheetReport.locationType).toEqual('absolute');
    expect(workSheetReport.location).toEqual('2,11:14,11');
    expect(workSheetReport.type).toEqual('table');
    expect(workSheetReport.inputs).toContain('APP_LOGGED_IN_USER_STATE');
    expect(workSheetReport.header.type).toEqual('fixed');
    expect(workSheetReport.header.location).toEqual('NA');
    expect(workSheetReport.header.reportQueries[0].dataSource).toEqual('ds');
    expect(workSheetReport.header.reportQueries[0].dataSourceType).toEqual('NA');
    expect(workSheetReport.header.reportQueries[0].query).toEqual('NA');
    expect(workSheetReport.data.location).toEqual('B11');
    expect(workSheetReport.data.reportQueries[0].dataSource).toEqual('ds');
    expect(workSheetReport.data.reportQueries[0].dataSourceType).toEqual('sql');
    expect(workSheetReport.data.reportQueries[0].query).toContain('SELECT');
    expect(workSheetReport.summary.type).toEqual('all');
    expect(workSheetReport.summary.level).toEqual('blank');
    expect(workSheetReport.summary.location).toEqual('2,12:13,12');
});
test('buildReport() test', async () => {
    const excelReportMetadata = new models_1.ExcelReport('CDP-14', '', []);
    const reportBuilder = new excel_report_builder_1.ExcelReportBuilder();
    // ToDo: Uable to mock, need to mock it properly
    const buildReportMock = sinon_1.default.mock(reportBuilder);
    buildReportMock.expects('build').returns('<Buffer 101 789 more bytes>');
    const reportOptions = new models_2.ReportOptions('CDP-14', '', {}, 'en');
    const excelBytes = await generators_1.metadataGenerator.buildReport(excelReportMetadata, reportOptions);
    const metaDataService = services_1.MetadataService.INSTANCE;
    expect(metaDataService.excelReportList.length).toEqual(1);
    // expect(excelBytes).toEqual('<Buffer 101 789 more bytes>');
    buildReportMock.restore();
});
//# sourceMappingURL=metadata.generator.test.js.map