"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errorhandler_1 = __importDefault(require("errorhandler"));
const info_commons_1 = require("info-commons");
const app_1 = __importDefault(require("./app"));
const index_1 = __importDefault(require("./config/index"));
const init_config_1 = require("./init-config");
init_config_1.AppengReportsConfig.configure(index_1.default.get('db'), info_commons_1.DIALECT);
if (process.env.NODE_ENV === 'development') {
    app_1.default.use(errorhandler_1.default());
}
let server;
if (process.env.NODE_ENV !== 'test') {
    server = app_1.default.listen(8987);
}
exports.default = server;
//# sourceMappingURL=server.js.map